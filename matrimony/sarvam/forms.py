from django.contrib.auth.forms import UserCreationForm
from sarvam.models import SarvamUser, Profile, Picture
from django import forms


class SarvamUserForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(SarvamUserForm, self).__init__(*args, **kwargs)
        self.fields['mobile'].widget.attrs.pop("autofocus", None)

    class Meta:
        model = SarvamUser
        fields = [
            'profile_for',
            'name',
            'dob',
            'gender',
            'email',
            'mobile',
            'password1',
            'password2'
        ]

        widgets = {
            'dob': forms.DateInput(format='%d-%m-%Y'),
        }

        labels = {
            'dob': 'Date of birth (dd-mm-yyyy):'
        }


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = [
            'caste',
            'height',
            'qualification',
            'occupation',
            'income',
            'contact_no',
            'contact_name',
            'address',
            'star',
            'rasi',
            'thisai',
            'father',
            'mother',
            'brothers',
            'sisters',
            'interest_caste'
        ]

        widgets = {
            'address': forms.Textarea(attrs={'class': 'materialize-textarea'})
        }

        labels = {
            'income': "Monthly Income",
            'father': "Father's Name",
            'mother': "Mother's Name",
        }


class PictureForm(forms.ModelForm):
    class Meta:
        model = Picture
        fields = ['pro_picture', 'jathagam']
