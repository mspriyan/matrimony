# Generated by Django 2.0.2 on 2018-07-26 19:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sarvam', '0005_auto_20180726_0155'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='address',
            field=models.TextField(max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='caste',
            field=models.CharField(choices=[('SV', 'Saiva Vellalar'), ('HV', 'Hindu Vellalar/Pillai'), ('SC', 'Saiva Chettiyar')], max_length=2, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='contact_name',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='contact_no',
            field=models.CharField(max_length=16, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='father',
            field=models.CharField(blank=True, max_length=70, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='income',
            field=models.CharField(help_text='per month', max_length=15, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='interest_caste',
            field=models.CharField(choices=[('SAME', 'Same as my caste'), ('OTHER', 'Any subcaste within Saivam or Vellalar')], max_length=5, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='jathagam',
            field=models.ImageField(null=True, upload_to='uploads/jathagam'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='mother',
            field=models.CharField(blank=True, max_length=70, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='occupation',
            field=models.CharField(max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='pro_picture',
            field=models.ImageField(null=True, upload_to='uploads/profile'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='qualification',
            field=models.CharField(max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='rasi',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='sarvam.Rasi'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='star',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='sarvam.Star'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='thisai',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='sarvam.Thisai'),
        ),
    ]
