# Generated by Django 2.0.6 on 2018-10-10 11:22

from django.db import migrations

def move_caste(apps, schema_editor):
    Profiles = apps.get_model('sarvam', 'Profile')
    Caste = apps.get_model('sarvam', 'Caste')
    for profile in Profiles.objects.all():
        if profile.caste == "SP":
            profile.caste_link = Caste.objects.get(name="Saiva Pillai")
        elif profile.caste == "SV":
            profile.caste_link = Caste.objects.get(name="Saiva Vellalar")
        elif profile.caste == "HV":
            profile.caste_link = Caste.objects.get(name="Hindhu Vellalar / Pillai")
        elif profile.caste == "SC":
            profile.caste_link = Caste.objects.get(name="Saiva Chettiyar")
        else:
            profile.caste_link = None
        profile.save()


class Migration(migrations.Migration):

    dependencies = [
        ('sarvam', '0015_profile_caste_link'),
    ]

    operations = [
        migrations.RunPython(move_caste),
    ]
