# Generated by Django 2.0.2 on 2018-07-21 08:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sarvam', '0006_auto_20180708_2225'),
    ]

    operations = [
        migrations.AddField(
            model_name='sarvamuser',
            name='mobile_verified',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='sarvamuser',
            name='gender',
            field=models.CharField(choices=[('M', 'Male'), ('F', 'Female')], default=None, max_length=1, null=True),
        ),
        migrations.AlterField(
            model_name='sarvamuser',
            name='profile_for',
            field=models.CharField(choices=[('MY', 'Myself'), ('SON', 'Son'), ('DAU', 'Daughter'), ('FRI', 'Friend'), ('REL', 'Relative')], default=None, max_length=4, null=True),
        ),
    ]
