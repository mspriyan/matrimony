from django.apps import AppConfig


class SarvamConfig(AppConfig):
    name = 'sarvam'

    def ready(self):
        import sarvam.signals