from django.contrib.auth import get_user_model
from django.db.models import Q
from django.core.exceptions import ValidationError


class SarvamBackend:

    def authenticate(self, request, username=None, password=None):
        usermodel = get_user_model()
        try:
            user = usermodel.objects.get(
                Q(mobile__iexact=username)
            )
            if user.check_password(password):
                return user
            else:
                return None
        except usermodel.DoesNotExist:
            raise ValidationError("Enter correct phone number and password")

    def get_user(self, user_id):
        usermodel = get_user_model()
        try:
            return usermodel.objects.get(pk=user_id)
        except usermodel.DoesNotExist:
            return None
