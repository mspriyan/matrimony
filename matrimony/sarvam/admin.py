from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from .models import Star, Rasi, Thisai, SarvamUser, Profile, Picture, Caste
from .forms import SarvamUserForm


class UserChangeForm(forms.ModelForm):
    """  A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    # password = ReadOnlyPasswordHashField(
    #     label=("Password"),
    #     help_text=("Raw passwords are not stored, so there is no way to see "
    #                "this user's password, but you can change the password "
    #                "using <a href=\"../password/\">this form</a>."))

    class Meta:
        model = SarvamUser
        fields = ('name', 'email', 'mobile', 'password', 'dob', 'is_admin',
                  'mobile_verified', 'profile_for')

        def clean_password(self):
            # Regardless of what the user provides, return the initial value.
            # This is done here, rather than on the field, because the
            # field does not have access to the initial value
            return self.initial["password"]


class ProfileInline(admin.StackedInline):
    model = Profile
    extra = 0


class PictureInline(admin.StackedInline):
    model = Picture
    extra = 0
    fields = ["pro_picture", "jathagam"]


class UserAdmin(BaseUserAdmin):

    inlines = [ProfileInline, PictureInline]
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = SarvamUserForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('name', 'dob', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = ((None, {
        'fields': ('mobile', 'email', 'password')
    }), ('Personal info', {
        'fields': ('name', 'dob', 'gender', 'mobile', 'mobile_verified',
                   'profile_for')
    }), ('Permissions', {
        'fields': ('is_admin', )
    }), ('Important Dates', {
        'fields': ('last_login', )
    }))
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = ((None, {
        'classes': ('wide', ),
        'fields': ('profile_for', 'name', 'dob', 'gender', 'mobile',
                   'mobile_verified', 'email', 'password1', 'password2')
    }), )
    search_fields = ('email', 'mobile')
    ordering = ('email',)
    filter_horizontal = ()


# Now register the new UserAdmin...
admin.site.register(SarvamUser, UserAdmin)
admin.site.register(Star)
admin.site.register(Rasi)
admin.site.register(Thisai)
admin.site.register(Profile)
admin.site.register(Picture)
admin.site.register(Caste)
