from django.db.models.signals import post_save
from sarvam.models import SarvamUser, Profile
from django.dispatch import receiver

@receiver(post_save, sender=SarvamUser)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()