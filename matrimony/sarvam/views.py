from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.conf import settings
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ValidationError
from django.views.decorators.cache import cache_page
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import SarvamUserForm, ProfileForm, PictureForm
from .models import SarvamUser
from .msgapi import send_otp, verify_otp, resend_otp
from payu.gateway import get_hash, check_hash
import json
from uuid import uuid4


@cache_page(60 * 15)
def index(request):
    """Index page of the site"""
    if request.user.is_authenticated:
        return redirect('home')
    return render(request, "sarvam/index.html")


def user_signup(request):
    form = SarvamUserForm()
    title = 'Create Account'

    if request.method == 'POST':
        form = SarvamUserForm(request.POST)
        if form.is_valid():
            form.save()
            passw = form.cleaned_data.get('password1')
            mobile = form.cleaned_data.get('mobile')
            user = authenticate(username=mobile, password=passw)
            login(request, user)
            if settings.DEBUG:
                user.mobile_verified = True
            return redirect('verify')

    return render(request, 'sarvam/signup.html', {'form': form, 'title': title})


# Mobile OTP verification view
@login_required
def user_verify(request, **kwargs):
    user = request.user
    mobile = user.mobile
    if request.method == 'POST':
        if 'resendotp' in request.POST:
            sms = resend_otp(mobile)
            return redirect('verify')
        elif 'verifyotp' in request.POST:
            otp = request.POST.get('otp')
            sms = verify_otp(mobile, otp)
            data = json.loads(sms.text)
            if data['type'] == 'success' or data['message'] == 'already_verified':
                if user.mobile_verified is False:
                    user.mobile_verified = True
                    user.save()
                    return redirect('profile')
            elif data['message'] == 'mobile_not_found':
                messages.add_message(request, messages.ERROR, "Please enter correct otp")
                return redirect('verify')
            else:
                messages.add_message(request, messages.ERROR, "There is an error sending otp, Please contact us")
                return redirect('verify')
    if user.mobile_verified:
        return redirect('home')
    try:
        sms = send_otp(mobile)
    except Exception as e:
        messages.add_message(
            request, messages.ERROR, 'Cant send Verification code, Please Register again')
        return redirect('signup')
    data = json.loads(sms.text)
    if data['type'] == 'success':
        messages.add_message(request, messages.SUCCESS, "An otp is send to this number")
    else:
        messages.add_message(request, messages.ERROR, "Can't send otp. Please check the number")
        return redirect('signup')
    return render(request, "sarvam/verify.html")


def user_login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        try:
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                if user.mobile_verified is False and user.is_admin is False:
                    return redirect('verify')
                else:
                    return redirect('home')
            else:
                messages.add_message(request, messages.ERROR, "Please Check your password")
                return redirect('login')
        except ValidationError:
            messages.add_message(request, messages.ERROR, "You don't have an account")

    return render(request, "sarvam/login.html", {'title': 'Login', })


@login_required(login_url='/login/')
def create_profile(request):
    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES)
        if form.is_valid():
            f = form.save(commit=False)
            f.user = request.user
            f.save()
            return redirect('uploads')
    form = ProfileForm()
    title = 'Add Profile'
    context = {
        'form': form,
        'title': title
    }
    return render(request, 'sarvam/signup.html', context=context)


@login_required(login_url='/login/')
def upload_image(request):
    if request.method == 'POST':
        form = PictureForm(request.POST, request.FILES)
        if form.is_valid:
            f = form.save(commit=False)
            f.user = request.user
            f.save()
            return redirect('home')
        else:
            messages.add_message(request, messages.ERROR, 'Please choose an appropirate image')
            return redirect('uploads')
    form = PictureForm()
    title = 'Upload Photo'
    context = {
        'form': form,
        'title': title
    }
    return render(request, 'sarvam/uploads.html', context=context)


@login_required(login_url='/login/')
def payment(request):
    if request.user.profile.is_paid is True:
        return redirect('home')
    name = request.user.name
    name = name.replace(" ", "")
    txnid = uuid4().hex[0:10]
    context = {
        'txnid': txnid,
        'amount': 500.00,
        'productinfo': 'Registration Fee',
        'firstname': name,
        'email': request.user.email,
    }
    hash = get_hash(context)
    context['key'] = settings.PAYU_MERCHANT_KEY
    context['hash'] = hash
    context['phone'] = request.user.mobile
    context['action'] = getattr(settings, 'PAYU_URL', None)
    context['surl'] = getattr(settings, 'PAYU_RESPONSE_URL', None)
    context['furl'] = getattr(settings, 'PAYU_RESPONSE_URL', None)

    return render(request, 'sarvam/payment.html', context=context)


@csrf_exempt
def pay_response(request):
    if request.method == "POST":
        stat = request.POST['status']
        mobile = request.POST['phone']
        if check_hash(request.POST) and stat == "success":
            profile = SarvamUser.objects.get(mobile=mobile).profile
            profile.is_paid = True
            profile.save()
            return render(request, "sarvam/pay_success.html")
        elif stat == "failure":
            return render(request, "sarvam/pay_failure.html")
    return redirect("home")


@login_required(login_url='/login/')
def user_home(request):
    user = request.user
    user_profile = SarvamUser.objects.get(pk=user.id).profile
    if user.is_admin is False:
        if user_profile.address is None:
            return redirect('profile')
        elif user_profile.is_paid is False:
            return redirect('payment')
    user_list = SarvamUser.objects.exclude(is_admin=True)
    paginator = Paginator(user_list, 9)
    page = request.GET.get('page', 1)
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
    context = {
        'users': users,
    }
    return render(request, 'sarvam/home.html', context)


@login_required(login_url='/login/')
def profile_detail(request, pk):
    info = get_object_or_404(SarvamUser, pk=pk)
    return render(request, "sarvam/detail.html", {'info': info})


@login_required(login_url='/login/')
def user_logout(request):
    logout(request)
    return redirect('index')
