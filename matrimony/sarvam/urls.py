from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('signup/', views.user_signup, name='signup'),
    path('verify/', views.user_verify, name='verify'),
    path('profile/', views.create_profile, name='profile'),
    path('uploads/', views.upload_image, name='uploads'),
    path('payment/', views.payment, name='payment'),
    path('response/', views.pay_response, name='response'),
    path('login/', views.user_login, name='login'),
    path('logout/', views.user_logout, name='logout'),
    path('home/', views.user_home, name='home'),
    path('detail/<int:pk>', views.profile_detail, name='detail'),
]
