import requests
from django.conf import settings
from random import randint


def send_otp(mobile):
    """Takes a mobile number and send an otp to that number"""
    url = "http://control.msg91.com/api/sendotp.php"
    otp = randint(1111, 9999)
    message = "Thanks for choosing SarvaMangalya, your otp is {{otp}}"
    payload = {
        'authkey': settings.MSG_AUTH_KEY,
        'mobile': mobile,
        'message': message.replace("{{otp}}", str(otp)),
        'sender': settings.SENDER,
        'otp': otp,
    }
    return requests.post(url, data=payload)


def resend_otp(mobile):
    """Resend OTP to a mobile number"""
    url = "http://control.msg91.com/api/retryOTP.php"
    payload = {
        'authkey': settings.MSG_AUTH_KEY,
        'mobile': mobile,
        'retrytype': 'text'
    }
    return requests.post(url, data=payload)


def verify_otp(mobile, otp):
    url = "https://control.msg91.com/api/verifyRequestOTP.php"
    payload = {
        'authkey': settings.MSG_AUTH_KEY,
        'mobile': mobile,
        'otp': otp
    }
    return requests.post(url, data=payload)


def send_message(message, mobile, password):
    """sends a message to new user"""
    url = "http://api.msg91.com/api/sendhttp.php"
    payload = {
        'authkey': settings.MSG_AUTH_KEY,
        'mobiles': mobile,
        'route': "4",
        'sender': "SARVAM",
        'country': "91",
        'message': message,
    }
    return requests.get(url, params=payload)
