from django.db import models
from django.contrib.auth.models import (
    PermissionsMixin, AbstractBaseUser, BaseUserManager
)
# from .utils import sarvam_inc


# Create your models here.
class Star(models.Model):
    name = models.CharField(max_length=25)

    def __str__(self):
        return self.name


class Rasi(models.Model):
    name = models.CharField(max_length=25)

    def __str__(self):
        return self.name


class Thisai(models.Model):
    name = models.CharField(max_length=25)

    def __str__(self):
        return self.name


class Caste(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class SarvamUserManager(BaseUserManager):

    def create_user(self, mobile, name, password):
        if not mobile:
            return ValueError('User must have a phone no')
        user = self.model(mobile=mobile, name=name)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, mobile, name, password):
        user = self.create_user(mobile, name, password)
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

    def get_by_natural_key(self, mobile_):
        return self.get(mobile=mobile_)


# Custom user class subclassing abstracbaseuser
class SarvamUser(AbstractBaseUser, PermissionsMixin):
    # basic info
    id = models.AutoField
    name = models.CharField(max_length=100)
    dob = models.DateField(null=True)
    gender_choices = (
        ('Male', 'Male'),
        ('Female', 'Female'),
    )
    gender = models.CharField(
        max_length=6, choices=gender_choices, null=True, default=None)
    mobile = models.CharField(max_length=16, unique=True)
    mobile_verified = models.BooleanField(default=False)
    email = models.EmailField(max_length=234, blank=True, null=True)
    pfor_choices = (
        ('MY', 'Myself'),
        ('SON', 'Son'),
        ('DAU', 'Daughter'),
        ('FRI', 'Friend'),
        ('REL', 'Relative'),
    )
    profile_for = models.CharField(
        max_length=4, choices=pfor_choices, null=True, default=None)
    # only for site admins
    is_admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'mobile'
    REQUIRED_FIELDS = ['name']
    objects = SarvamUserManager()

    def get_short_name(self):
        return self.name

    def natural_key(self):
        return self.mobile

    def __str__(self):
        return self.name

    @property
    def is_staff(self):
        # Simplest possible answer: All admins are staff
        return self.is_admin


class Profile(models.Model):
    user = models.OneToOneField(
        SarvamUser,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name='profile')
    is_paid = models.BooleanField(default=False)
    sarvam_id = models.CharField(
        max_length=20, null=True, unique=True, editable=False)

    caste = models.ForeignKey(Caste, on_delete=models.PROTECT, null=True)
    # body info
    height = models.FloatField(null=True)
    # work and qualification
    qualification = models.CharField(max_length=300, null=True)
    occupation = models.CharField(max_length=300, null=True)
    income = models.CharField(max_length=15, help_text='per month', null=True)
    # contact info
    address = models.TextField(max_length=256, null=True)
    contact_no = models.CharField(max_length=16, null=True)
    contact_name = models.CharField(max_length=100, null=True)
    # Horoscope details
    star = models.ForeignKey(Star, null=True, on_delete=models.PROTECT)
    rasi = models.ForeignKey(Rasi, null=True, on_delete=models.PROTECT)
    thisai = models.ForeignKey(Thisai, null=True, on_delete=models.PROTECT)
    # family details
    father = models.CharField(max_length=70, blank=True, null=True)
    mother = models.CharField(max_length=70, blank=True, null=True)
    brothers = models.IntegerField(null=True)
    sisters = models.IntegerField(null=True)
    interest_caste_choices = (
        ('SAME', 'Same as my caste'),
        ('OTHER', 'Any subcaste within Saivam or Vellalar'),
    )
    interest_caste = models.CharField(
        max_length=5, choices=interest_caste_choices, null=True, default=None
    )

    def __str__(self):
        return self.user.name


class Picture(models.Model):
    user = models.OneToOneField(
        SarvamUser,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name='image')
    pro_picture = models.ImageField(
        upload_to='uploads/profile', blank=True, null=True)
    jathagam = models.ImageField(
        upload_to='uploads/jathagam', blank=True, null=True)

    def __str__(self):
        return self.user.name
