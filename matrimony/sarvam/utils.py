def sarvam_inc(last_id):
    """creates an custom id for users"""
    inc_id = int(last_id) + 1
    str_id = str(inc_id)
    return "SM" + str_id.zfill(5-len(str_id))


def create_otp_message(name, mobile, password):

    """Creates a welcome message for
    sarvamangalya Users created by admin
    with their username and password.
    """

    message = f"""
    Dear {name}, Thankyou for choosing Sarvamangalya Matrimony.
    Use the following details to login to your account.
    Username: {mobile} , Password: {password} at sarvamangalya.com/login.
    """
    return message
