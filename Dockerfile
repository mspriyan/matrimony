
FROM ubuntu:18.04
RUN mkdir /app
WORKDIR /app
RUN apt update -y && apt install python3 python3-pip python3-dev python3-setuptools \
build-essential -y && rm -r /var/lib/apt/lists/*
COPY requirements.txt /app/
RUN pip3 install -r requirements.txt
COPY . /app

