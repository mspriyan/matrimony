#!/bin/bash
if [ $DEBUG = 'True' ];
then
    python3 matrimony/manage.py makemigrations --settings=matrimony.local_settings;
    python3 matrimony/manage.py migrate --settings=matrimony.local_settings;
    python3 matrimony/manage.py collectstatic --no-input --settings=matrimony.local_settings; 
    uwsgi --http :8000 --chdir matrimony --master --processes 3 --module matrimony.local_wsgi:application;
else
    python3 matrimony/manage.py migrate;
    python3 matrimony/manage.py collectstatic --no-input;
    uwsgi --http :8000 --chdir matrimony --master --processes 3 --module matrimony.wsgi:application
fi